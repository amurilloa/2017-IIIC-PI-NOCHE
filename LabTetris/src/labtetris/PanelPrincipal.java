/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labtetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelPrincipal extends JPanel implements KeyListener {

    private Logica log;

    public PanelPrincipal() {
        //Tamaño al panel
        setPreferredSize(new Dimension(900, 900));
        //Borde 
        //setBorder(BorderFactory.createLineBorder(Color.BLACK, 6));
        addKeyListener(this);
        setFocusable(true);
        log = new Logica();
        log.setFondo(900);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //Fondo
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        log.paint(g);
        log.mover();
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            log.mover(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            log.mover(2);
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            log.rotar();
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            log.rotar();
        } else if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            log.setSpeed(40);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            log.setSpeed(5);
        }

    }
}
