/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labtetris;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author ALLAN
 */
public class Figura {

    private int x;
    private int y;
    private Color color;
    private int tipo; //0 Cuadrado, 1: Rectangulo
    private boolean moviendo;
    private boolean rotar;

    public Figura() {
    }

    public Figura(int x, int y, Color color, int tipo) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.tipo = tipo;
    }

    public Rectangle getBounds() {
        if (tipo == 0) {
            return new Rectangle(x, y, 60, 60).getBounds();
        } else if (tipo == 1) {
            if (!rotar) {
                return new Rectangle(x, y, 120, 30).getBounds();
            } else {
                return new Rectangle(x, y, 30, 120).getBounds();
            }
        }
        return new Rectangle(0, 0, 1, 1).getBounds();
    }

    public void pintarFigura(Graphics g) {
        g.setColor(color);
        if (tipo == 0) {
            g.fillRect(x, y, 60, 60);
            g.setColor(Color.WHITE);
            g.drawRect(x, y, 60, 60);
            g.drawLine(x + 30, y, x + 30, y + 60);
            g.drawLine(x, y + 30, x + 60, y + 30);
        } else if (tipo == 1) {
            if (!rotar) {
                g.fillRect(x, y, 120, 30);
                g.setColor(Color.WHITE);
                g.drawRect(x, y, 120, 30);
                g.drawLine(x + 30, y, x + 30, y + 30);
                g.drawLine(x + 60, y, x + 60, y + 30);
                g.drawLine(x + 90, y, x + 90, y + 30);
            } else {
                g.fillRect(x, y, 30, 120);
                g.setColor(Color.WHITE);
                g.drawRect(x, y, 30, 120);
                g.drawLine(x, y + 30, x + 30, y + 30);
                g.drawLine(x, y + 60, x + 30, y + 60);
                g.drawLine(x, y + 90, x + 30, y + 90);
            }
        }
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public boolean isMoviendo() {
        return moviendo;
    }

    public void setMoviendo(boolean moviendo) {
        this.moviendo = moviendo;
    }

    public boolean isRotar() {
        return rotar;
    }

    public void setRotar(boolean rotar) {
        this.rotar = rotar;
    }

}
