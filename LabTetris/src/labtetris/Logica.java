/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labtetris;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
 
/**
 *
 * @author ALLANl
 */
public class Logica {

    private LinkedList<Figura> figuras;
    private int fondo;
    private int speed;

    public Logica() {
        figuras = new LinkedList<>();
        figuras.add(new Figura(350, -30, Color.RED, 0));
        figuras.get(0).setMoviendo(true);
        speed = 10;

    }

    /**
     * Punta el estado del juego
     * @param g "Panel o hoja donde voy a dibujar"
     */
    public void paint(Graphics g) {
        for (Figura figura : figuras) {
            figura.pintarFigura(g);
        }
    }

    /**
     * Mover automático, hacia abajo
     * Se encarga de modificar el Y de las figuras (Y + speed)
     * siempre que la figura se este moviendo (solo 1 se mueve en la lista)
     * y que no se tenga que detener(se detiene si se pasa del fondo o si choca 
     * con otra)
     */
    public void mover() {
        for (int x = 0; x < figuras.size(); x++) {
            if (figuras.get(x).isMoviendo()) {
                if (detenerFigura(figuras.get(x))) {
                    figuras.get(x).setMoviendo(false);
                    int random = (int) (Math.random() * 10);
                    if (random % 2 == 0) {
                        Figura fig = new Figura(350, -30, Color.RED, 0);
                        fig.setMoviendo(true);
                        figuras.add(fig);
                    } else {
                        Figura fig = new Figura(350, -30, Color.RED, 1);
                        fig.setMoviendo(true);
                        figuras.add(fig);
                    }
                } else {
                    figuras.get(x).setY(figuras.get(x).getY() + speed);
                }
                break;
            }
        }
    }

    /**
     * Mover manual, hacia los lados
     * Se encarca de mover la figura en dirección izquierda y derecha, la figura
     * que se mueve es la que este en estado moviendose
     * @param i int 1 para 
     */
    public void mover(int i) {
        for (int x = 0; x < figuras.size(); x++) {
            if (figuras.get(x).isMoviendo()) {
                if (i == 1) {
                    figuras.get(x).setX(figuras.get(x).getX() + 10);
                } else if (i == 2) {
                    figuras.get(x).setX(figuras.get(x).getX() - 10);
                }
                break;
            }
        }
    }

    public void rotar() {
        for (int x = 0; x < figuras.size(); x++) {
            if (figuras.get(x).isMoviendo()) {
                figuras.get(x).setRotar(!figuras.get(x).isRotar());
                break;
            }
        }
    }

    public boolean detenerFigura(Figura fig) {
        if (fig.getTipo() == 0) {
            if (fig.getY() + 60 + 10 >= fondo) {
                return true;
            }
        } else if (fig.getTipo() == 1) {
            if (!fig.isRotar()) {
                if (fig.getY() + 30 + 10 >= fondo) {
                    return true;
                }
            } else {
                if (fig.getY() + 120 + 10 >= fondo) {
                    return true;
                }
            }
        }
        fig.setY(fig.getY() + speed);
        for (Figura figura : figuras) {
            if (!figura.isMoviendo() && figura.getBounds().intersects(fig.getBounds())) {
                fig.setY(fig.getY() - speed);
                return true;
            }
        }
        fig.setY(fig.getY() - speed);
        return false;
    }

    public void setFondo(int fondo) {
        this.fondo = fondo;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
