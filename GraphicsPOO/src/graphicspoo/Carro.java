/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoo;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Carro {

    //Estado del objeto
    private Color color;
    private boolean estaEncendido;
    private String placa;

    //Atributos Gráficas
    private int x;
    private int y;
    private int tamano;//1, 2, 3
    private int direccion;//0 Derecha, 1: Izquierda

    public Carro(int tam) {
        color = Color.BLACK;
        tamano = tam;
    }

    public Carro(Color color, boolean estaEncendido, String placa, int x, int y, int tamano, int direccion) {
        this.color = color;
        this.estaEncendido = estaEncendido;
        this.placa = placa;
        this.x = x;
        this.y = y;
        this.tamano = tamano;
        this.direccion = direccion;
    }

    public void pintar(Graphics g) {
        g.setColor(color);

        //Carrocería
        int[] xs = {150, 250, 250, 300, 320, 390, 390, 150};
        int[] ys = {150, 150, 120, 120, 150, 165, 195, 195};

        for (int i = 0; i < xs.length; i++) {
            xs[i] *= tamano;
            xs[i] += x;
            ys[i] *= tamano;
            ys[i] += y;
        }
        g.fillPolygon(xs, ys, xs.length);

        //Bumpers
        g.fillRect(x + 140 * tamano, y + 185 * tamano, 10 * tamano, 10 * tamano);
        g.fillRect(x + 390 * tamano, y + 185 * tamano, 5 * tamano, 10 * tamano);

        //Llantas
        g.setColor(Color.GRAY);
        g.fillOval(x + 175 * tamano, y + 175 * tamano, 40 * tamano, 40 * tamano);
        g.fillOval(x + 335 * tamano, y + 175 * tamano, 40 * tamano, 40 * tamano);

        //Luces
        if (estaEncendido) {
            g.setColor(Color.RED);
            g.fillRect(x + 151 * tamano, y + 151 * tamano, 5 * tamano, 15 * tamano);
            g.setColor(Color.YELLOW);
            g.fillRect(x + 384 * tamano, y + 166 * tamano, 5 * tamano, 5 * tamano);
        } else {
            g.setColor(Color.GRAY);
            g.fillRect(x + 151 * tamano, y + 151 * tamano, 5 * tamano, 15 * tamano);
            g.fillRect(x + 384 * tamano, y + 166 * tamano, 5 * tamano, 5 * tamano);
        }

    }

    public void setEstaEncendido(boolean estaEncendido) {
        this.estaEncendido = estaEncendido;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

}
