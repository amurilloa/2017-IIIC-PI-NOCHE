/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelPOO extends JPanel {

   
    public PanelPOO() {
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 500);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //Empezamos a pintar

        Carro c1 = new Carro(2);
        c1.setEstaEncendido(true);
        c1.setX(1);
        c1.setY(1);
        c1.pintar(g);

        c1 = new Carro(3);
        c1.setEstaEncendido(true);
        c1.setX(-150);
        c1.setY(400);
        c1.pintar(g);

    }

}
