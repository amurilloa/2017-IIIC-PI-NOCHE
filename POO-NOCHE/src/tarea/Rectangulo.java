/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Rectangulo {

    private double ancho;
    private double largo;

    public Rectangulo() {
    }

    public Rectangulo(double ancho, double largo) {
        this.ancho = ancho;
        this.largo = largo;
    }

    /**
     * Calcula el área del rectangulo
     *
     * @return double área del rectangulo
     */
    public double calcularArea() {
        double area = largo * ancho;
        return Math.round(area * 100) / 100.0;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public double getLargo() {
        return largo;
    }

    public void setLargo(double largo) {
        this.largo = largo;
    }

    @Override
    public String toString() {
        return "Rectangulo{" + "ancho=" + ancho + ", largo=" + largo + '}';
    }

}
