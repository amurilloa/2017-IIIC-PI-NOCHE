/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Fecha {

    private int dia;
    private int mes;
    private int anno;

    public Fecha() {
        dia = 1;
        mes = 1;
        anno = 1900;
    }

    public Fecha(int dia, int mes, int anno) {
        if (esValida(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {
            this.dia = 1;
            this.mes = 1;
            this.anno = 1900;
        }
    }

    /**
     * Modifica el valor de la fecha
     *
     * @param dia int dia a asignar
     * @param mes int mes a asignar
     * @param anno int año a asignar
     */
    public void setFecha(int dia, int mes, int anno) {
        if (esValida(dia, mes, anno)) {
            this.dia = dia;
            this.mes = mes;
            this.anno = anno;
        } else {                                                            
            this.dia = 1;
            this.mes = 1;
            this.anno = 1900;
        }
    }

    /**
     * Retorna la fecha con un formato DD/MM/YYYY
     *
     * @return String con la fecha
     */
    public String getFecha() {
        String d = String.valueOf(dia);
        if (dia < 10) {
            d = "0" + d;
        }
        //If corto o de asignación 
        String m = mes < 10 ? "0" + mes : String.valueOf(mes);
        return d + "/" + m + "/" + anno;
    }

    /**
     * Retorna la fecha en formato largo
     *
     * @return String fecha
     */
    public String getFechaLarga() {
        //1 de noviembre de 2017
        return dia + " de " + convertir(mes) + " de " + anno;
    }

    /**
     * Convierte el mes(entero) a letras
     *
     * @param mes int mes a convertir
     * @return String mes en letrass
     */
    private String convertir(int mes) {
        switch (mes) {
            case 1:
                return "Enero";
            case 2:
                return "Febrero";
            case 3:
                return "Marzo";
            case 4:
                return "Abril";
            case 5:
                return "Mayo";
            case 6:
                return "Junio";
            case 7:
                return "Julio";
            case 8:
                return "Agosto";
            case 9:
                return "Septiembre";
            case 10:
                return "Octubre";
            case 11:
                return "Noviembre";
            case 12:
                return "Diciembre";
            default:
                return "Desconocido";
        }
    }

    private boolean esValida(int dia, int mes, int anno) {
        if (dia <= 0 || dia > 31) {
            return false;
        } else if (mes <= 0 || mes > 12) {
            return false;
        } else if (anno < 0) {
            return false;
        } else if (dia > 29 && mes == 2 && esBisiesto(anno)) {
            return false;
        } else if (dia > 28 && mes == 2 && !esBisiesto(anno)) {
            return false;
        } else if (dia > 30 && (mes == 4 || mes == 6 || mes == 9 || mes == 11)) {
            return false;
        }
        return true;
    }

    /**
     * Valida si un año es Bisiciesto
     *
     * @param anno int año a validar
     * @return true si el año es bisiesto, false caso contrario
     */
    private boolean esBisiesto(int anno) {
        if (anno % 4 == 0) {
            if (anno % 100 == 0) {
                return anno % 400 == 0;
            }
            return true;
        }
        return false;
    }
}
