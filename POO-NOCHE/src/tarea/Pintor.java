/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author ALLAN
 */
public class Pintor {

    public static void main(String[] args) {

        String menu = "\n    EL PINTOR v0.01\n"
                + "1. Nueva cotización\n"
                + "2. Salir\n"
                + "Seleccione una opción";

        SAL_APP:
        while (true) {
            int op = Util.leerInt(menu);
            switch (op) {
                case 2:
                    System.out.println("Gracias por utilizar la aplicación.");
                    break SAL_APP;
                case 1:
                    String nombre = Util.leerTexto("Cliente");
                    double areaPar = 0;
                    double areaVen = 0;
                    while (true) {
                        Rectangulo pared = new Rectangulo();
                        System.out.println("\nDatos de la Pared:");
                        pared.setAncho(Util.leerDouble("- Ancho"));
                        pared.setLargo(Util.leerDouble("- Alto"));
                        areaPar += pared.calcularArea();
                        double areaV = 0;
                        System.out.println("- Área: " + pared.calcularArea() + "m²");
                        SALIR_VEN:
                        while (true) {
                            int tip = Util.leerInt("\nTipo de Ventana\n"
                                    + "1. Circular\n"
                                    + "2. Rectangular\n"
                                    + "3. No tiene/No hay más\n"
                                    + "Seleccione una opción");
                            switch (tip) {
                                case 3:
                                    break SALIR_VEN;
                                case 1: {
                                    System.out.println("Datos de la ventana: ");
                                    Circunferencia v = new Circunferencia();
                                    double diametro = Util.leerDouble("- Diámetro");
                                    v.setRadio(diametro / 2);
                                    if (areaV + v.calcularArea() > pared.calcularArea()) {
                                        System.out.println("Error, área de la ventana excede el área de la pared");
                                    } else {
                                        areaV += v.calcularArea();
                                        areaVen += v.calcularArea();
                                    }
                                    System.out.println("- Área: " + v.calcularArea());
                                    break;
                                }
                                default: {
                                    System.out.println("Datos de la ventana: ");
                                    Rectangulo v = new Rectangulo();
                                    v.setAncho(Util.leerDouble("- Ancho"));
                                    v.setLargo(Util.leerDouble("- Alto"));
                                    if (areaV + v.calcularArea() > pared.calcularArea()) {
                                        System.out.println("Error, área de la ventana excede el área de la pared");
                                    } else {
                                        areaV += v.calcularArea();
                                        areaVen += v.calcularArea();
                                    }
                                    System.out.println("- Área: " + v.calcularArea());
                                    break;
                                }
                            }
                        }

                        int con = Util.leerInt("\nDesea pintar otra pared:\n"
                                + "1. Si\n"
                                + "2. No\n"
                                + "Seleccione una opción ");
                        if (con == 2) {
                            break;
                        }
                    }
                    // Imprimir la cotización
                    System.out.println("\n    COTIZACIÓN");
                    System.out.println("Cliente: " + nombre);
                    System.out.println("Área del lugar");
                    System.out.println("Área de paredes: " + areaPar + "m²");
                    System.out.println("Área de ventanas: " + areaVen + "m²");
                    double areaPin = areaPar - areaVen;
                    System.out.println("Área a pintar: " + areaPin + "m²");
                    int m = (int) Math.round(areaPin * 10);
                    int h = m / 60;
                    m %= 60;
                    System.out.println("Tiempo Aproximado: " + h + "hrs:" + m + "min");
                    double horas = h;
                    if (m > 0 && m <= 30) {
                        horas += 0.5;
                    } else if (m > 30) {
                        horas++;
                    }
                    System.out.println("Total : $" + (horas * 30));
                    //Nueva cotización.
                    break;
                default:
                    System.err.println("Opción Inválida, favor intente nuevamente");
                    break;
            }
        }
    }

}
