/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.noche;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class PruebaGarrobo {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static double leerDouble(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Double.parseDouble(sc.nextLine());
    }

    public static String leerTexto(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        String menu = "\n     MENU PRINCIPAL\n"
                + "1. Configuración\n"
                + "2. Ingresar al modo niños\n"
                + "3. Salir\n"
                + "Seleccione una opción";

        String menuC = "\n      MENU CONFIGURACIÓN\n"
                + "1. Garrobo 1\n"
                + "2. Garrobo 2\n"
                + "3. Volver\n"
                + "Seleccione una opción";

        String menuN = "\n    MENU DE NIÑOS\n"
                + "1. Garrobo 1 - Calc Distancia\n"
                + "2. Garrobo 2 - Calc Distancia\n"
                + "3. Garrobo 1 - Calc Tiempo\n"
                + "4. Garrobo 2 - Calc Tiempo\n"
                + "Seleccione una opción";

        final int PIN = 9320;

        Garrobo g1 = new Garrobo();
        Garrobo g2 = new Garrobo();
        

        while (true) {
            int op = leerInt(menu);
            if (op == 3) {
                System.out.println("Gracias por utilizar la aplicación");
                break;
            } else if (op == 1) {
                while (true) {
                    op = leerInt(menuC);
                    if (op == 3) {
                        break;
                    } else if (op == 1) {
                        g1.setNombre(leerTexto("Nombre"));
                        g1.setDistObs(leerDouble("Distancia"));
                        g1.setTiempoObs(leerInt("Tiempo"));
                        System.out.println("Configuración: " + g1);
                    } else if (op == 2) {
                        g2.setNombre(leerTexto("Nombre"));
                        g2.setDistObs(leerDouble("Distancia"));
                        g2.setTiempoObs(leerInt("Tiempo"));
                        System.out.println("Configuración: " + g2);
                    }
                }
            } else if (op == 2) {
                //Validaciones
                if (g1.getTiempoObs() > 0 && g2.getTiempoObs() > 0) {
                    while (true) {
                        op = leerInt(menuN);
                        if (op == PIN) {
                            break;
                        } else if (op == 1) {
                            double dis = leerDouble("Distancia a consultar");
                            double tie = g1.calcularTiempo(dis);
                            System.out.println("Hola amigito, mi nombre es " + g1.getNombre()
                                    + " yo tardo " + tie + "s en recorrer " + dis + "m");
                        } else if (op == 2) {
                            double dis = leerDouble("Distancia a consultar");
                            double tie = g2.calcularTiempo(dis);
                            System.out.println("Hola amigito, mi nombre es " + g2.getNombre()
                                    + " yo tardo " + tie + "s en recorrer " + dis + "m");
                        } else if (op == 3) {
                            int tie = leerInt("Tiempo a consultar");
                            double dis = g1.calcularDistancia(tie);
                            System.out.println("Hola amigito, mi nombre es " + g1.getNombre()
                                    + " yo recorro " + dis + "m en " + tie + "s");
                        } else if (op == 4) {
                            int tie = leerInt("Tiempo a consultar");
                            double dis = g2.calcularDistancia(tie);
                            System.out.println("Hola amigito, mi nombre es " + g2.getNombre()
                                    + " yo recorro " + dis + "m en " + tie + "s");
                        }
                    }
                } else {
                    System.out.println("Garrobos desconfigurados!!");
                }
            } else {
                System.out.println("Opción Inválida, intente nuevamente");
            }
        }
    }

}
