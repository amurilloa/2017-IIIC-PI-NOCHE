/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.noche;

/**
 *
 * @author ALLAN
 */
public class Garrobo {

    //Atributos
    private String nombre;
    private double distObs;
    private int tiempoObs;

    //Constructores
    public Garrobo() {
    }

    public Garrobo(String nombre, double distObs, int tiempoObs) {
        this.nombre = nombre;
        this.distObs = distObs;
        this.tiempoObs = tiempoObs;
    }

    //Métodos
    public double calcularDistancia(int tiempo) {
        // t*v = d
        double dis = velocidad() * tiempo;
        return Math.round(dis * 100) / 100;
    }

    public double calcularTiempo(double distancia) {
        double tie = distancia / velocidad();
        return Math.round(tie * 100) / 100;
    }

    private double velocidad() {
        return distObs / tiempoObs;
    }

    //Getters && Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDistObs() {
        return distObs;
    }

    public void setDistObs(double distObs) {
        this.distObs = distObs;
    }

    public int getTiempoObs() {
        return tiempoObs;
    }

    public void setTiempoObs(int tiempoObs) {
        this.tiempoObs = tiempoObs;
    }

    @Override
    public String toString() {
        return "Garrobo{" + "nombre=" + nombre + ", distObs=" + distObs + ", tiempoObs=" + tiempoObs + '}';
    }
}
