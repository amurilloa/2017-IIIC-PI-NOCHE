/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author ALLAN
 */
public class Logica {

    public int canMayus(String texto) {
        int can = 0;
        for (int i = 0; i < texto.length(); i++) {
            if (Character.isUpperCase(texto.charAt(i))) {
                can++;
            }
        }
        return can;
    }

    /**
     * Cantidad de vocales en una oración
     *
     * @param texto
     * @return
     */
    public int canVocales(String texto) {
        return texto.toUpperCase().replaceAll("[^AEIOU]", "").length();
    }

    /**
     * Cantidad de espacios en una oración
     *
     * @param texto
     * @return
     */
    public int canEspacios(String texto) {
        return texto.replaceAll("[^\\s]", "").length();
    }

    /**
     * Eliminar los espacios de una oración
     *
     * @param texto
     * @return
     */
    public String eliminarEspacios(String texto) {
        return texto.replaceAll("[\\s]", "");
    }

    public String primeraLetra(String texto) {
        //hola mundo
        //Hola Mundo
        String nuevo = "";
        boolean ant = true;
        for (int i = 0; i < texto.length(); i++) {
            if (ant) {
                nuevo += Character.toUpperCase(texto.charAt(i));
                ant = false;
            } else if (texto.charAt(i) == ' ') {
                ant = true;
                nuevo += texto.charAt(i);
            } else {
                nuevo += texto.charAt(i);
            }
        }
        return nuevo;
    }

}

//
// 
//
//Primera letra de cada palabra en máyuscula

//hola mundo = Hola Mundo
