/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manipulacioncaracteres;

/**
 *
 * @author ALLAN
 */
public class ManipulacionCaracteres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // |h|o|l|a| |m|u|n|d|o|
        // |0|1|2|3|4|5|6|7|8|9|       
        String texto = "Hola ÑuÑdÓ";
        char letra = 'a';
        
        System.out.println("Largo: " + texto.length());

        //Comparar Strings 
        System.out.println("Hola Mundo".equals(texto));
        System.out.println("Hola Ñundo".equalsIgnoreCase(texto));

        //Obtener una letra de un indice específico
        System.out.println(texto.charAt(5));

        //Invertir una palabra
        // "a"+"b" = "ab"
        // "b"+"a" = "ba"
        String nuevo = "";
        for (int i = texto.length() - 1; i >= 0; i--) {
            nuevo += texto.charAt(i);
        }
        System.out.println(nuevo);
        
        nuevo = "";
        for (int i = 0; i < texto.length(); i++) {
            nuevo = texto.charAt(i) + nuevo;
        }
        System.out.println(nuevo);

        //Letras a mayuscula y letras a minuscula
        System.out.println("Mayúscula: " + texto.toUpperCase());
        System.out.println("Minúscula: " + texto.toLowerCase());
        
        String usuario = " amurilloa ";
        String password = "123      ";
        System.out.println(usuario);
        //Elimina los espacios en blanco del principio y final del string
        System.out.println(usuario.trim());
        
        char[] arreglo = texto.toCharArray();
        arreglo[0] = 'X';
        nuevo = new String(arreglo);
        System.out.println(nuevo);
        //texto = texto.toUpperCase();
        System.out.println(texto);

        //Contar la mayusculas de una frase
        Logica log = new Logica();
        System.out.println(log.canMayus(texto));
        System.out.println("Cant Vocales: " + log.canVocales("Hola Mundo"));
        System.out.println("Cant Espacios: " + log.canEspacios("Ho la Mun do"));
        System.out.println("Eliminar Espacios: " + log.eliminarEspacios("Ho la Mun do"));
        System.out.println("Primera letra: " + log.primeraLetra("ho la mun do"));
        
        //Si una letra o frase esta en el texto original. 
        System.out.println(texto.contains("Ho"));
        System.out.println(texto.replace("Ñ", "M"));
        System.out.println(texto.replaceAll("[a-z]", "-"));
        
        
        
    }
    
}
