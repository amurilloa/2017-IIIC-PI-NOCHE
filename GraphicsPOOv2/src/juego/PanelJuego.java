/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelJuego extends JPanel implements KeyListener {

    private Logica log;

    public PanelJuego() {
        setPreferredSize(new Dimension(700, 700));
        //setBorder(BorderFactory.createLineBorder(Color.BLACK, 6));
        addKeyListener(this);
        setFocusable(true);
        log = new Logica();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        log.paintLaberito(g);
        log.moverPato();
        log.getPato().paint(g);
        if (log.getPato().getX() >= 670) {
            log.setMover(2);
        } else if (log.getPato().getX() <= 0) {
            log.setMover(1);
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            log.setMover(2);
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            log.setMover(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            log.setMover(3);
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            log.setMover(4);
        } else if (ke.getKeyCode() == KeyEvent.VK_S) {
            log.frenar();
        } else if (ke.getKeyCode() == KeyEvent.VK_A) {
            log.acelerar();
        }
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
