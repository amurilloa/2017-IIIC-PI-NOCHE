/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Cuadrado pato;
    private int mover;
    private int speed;
    private Cuadrado[][] muros;
    private String[][] lab = {
        {"-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-"},
        {" ", " ", "-", " ", " ", " ", "-", "-"},
        {"-", " ", "-", " ", "-", " ", "-", "-"},
        {"-", " ", " ", " ", "-", " ", "-", "-"},
        {"-", "-", " ", "-", "-", " ", "-", "-"},
        {"-", " ", " ", "-", "-", " ", " ", " "},
        {"-", "-", "-", "-", "-", "-", "-", "-"}};

    public Logica() {
        pato = new Cuadrado(30, Color.RED, true, 10, 20);
        muros = new Cuadrado[8][8];
        speed = 3;

    }

    public void acelerar() {
        speed++;
    }

    public void frenar() {
        if (speed > 0) {
            speed--;
        }
    }

    public void paintLaberito(Graphics g) {
        int y = 0;
        for (int f = 0; f < lab.length; f++) {
            int x = 0;
            for (int c = 0; c < lab[f].length; c++) {
                if (lab[f][c].equals("-")) {
                    g.setColor(Color.BLACK);
                    g.fillRect(x, y, 50, 50);
                } else if (lab[f][c].equals(" ") || lab[f][c].equals("p")) {
                    g.setColor(Color.WHITE);
                    g.fillRect(x, y, 50, 50);
                }
                x += 50;
            }
            y += 50;
        }
    }

    public void setMover(int mover) {
        this.mover = mover;
    }

    public void moverPato() {
        if (mover == 1) {
            pato.setX(pato.getX() + speed);
        } else if (mover == 2) {
            pato.setX(pato.getX() - speed);
        } else if (mover == 3) {
            pato.setY(pato.getY() - speed);
        } else if (mover == 4) {
            pato.setY(pato.getY() + speed);
        }
    }

    public Cuadrado getPato() {
        return pato;
    }

}
