/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelPrincipal extends JPanel implements KeyListener {

    private Rectangulo fondo;

    public PanelPrincipal() {
        setPreferredSize(new Dimension(350, 500));
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 6));
        addKeyListener(this);
        setFocusable(true);
        fondo = new Rectangulo(338, 488, Color.GRAY, true, 6, 6);
    }

    @Override //Sobreescribir el comportamiento del método 
    public void paint(Graphics g) {
        super.paint(g);

        Cuadrado c1 = new Cuadrado(100, Color.BLUE, true, 50, 50);
        Cuadrado c2 = new Cuadrado(100, Color.WHITE, true, 200, 50);
        Rectangulo r1 = new Rectangulo(100, 50, Color.CYAN, true, 50, 200);
        Circulo ci1 = new Circulo(25, Color.GREEN, true, 200, 200);
        Triangulo t1 = new Triangulo(100, 150, Color.ORANGE, true, 150, 450);
        fondo.paint(g);
        c1.paint(g);
        c2.paint(g);
        r1.paint(g);
        ci1.paint(g);
        t1.paint(g);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_1) {
            fondo.setColor(Color.GRAY);
        } else if (ke.getKeyCode() == KeyEvent.VK_2) {
            fondo.setColor(Color.YELLOW);
        } else if (ke.getKeyCode() == KeyEvent.VK_3) {
            fondo.setColor(Color.BLUE);
        } else if (ke.getKeyCode() == KeyEvent.VK_4) {
            fondo.setColor(Color.GREEN);
        } else if (ke.getKeyCode() == KeyEvent.VK_5) {
            fondo.setColor(Color.RED);
        }
        
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
