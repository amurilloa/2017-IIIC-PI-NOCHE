/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphicspoov2;

import java.awt.Frame;
import javax.swing.JFrame;

/**
 *
 * @author ALLAN
 */
public class GraphicsPOOv2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int[][] m = {
            {1, 3, 4},
            {4, 2, 5}};

        int[][] m2 = new int[2][3];
        m2[0][0] = 32;
        m2[0][1] = 32;
        m2[0][2] = 32;
        m2[1][0] = 32;
        m2[1][1] = 32;
        m2[1][2] = 32;

        String[][] lab = {
        {"-", "-", "-", "-", "-", "-", "-", "-"},
        {"x", "-", "-", "-", "-", "-", "-", "-"},
        {" ", "p", "-", " ", "-", "-", "-", "-"},
        {"x", " ", "-", " ", "-", "-", "-", "-"},
        {"-", " ", " ", " ", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-"},
        {"-", "-", "-", "-", "-", "-", "-", "-"}};

        for (int f = 0; f < lab.length; f++) {
            for (int c = 0; c < lab[f].length; c++) {
                System.out.print(lab[f][c] + " ");
            }
            System.out.println("");
        }

        JFrame frm = new JFrame("Demo - POO v2.0");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frm.setExtendedState(Frame.MAXIMIZED_BOTH); //Maximizar la ventana
        //Crear panel y agregarlo al formulario
        frm.add(new PanelPrincipal());
        frm.pack();
        frm.setLocationRelativeTo(null);
        frm.setVisible(true);
    }

}
