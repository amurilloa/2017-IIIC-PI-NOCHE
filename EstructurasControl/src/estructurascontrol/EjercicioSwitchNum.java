/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioSwitchNum {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static void main(String[] args) {
        int n = leerInt("Digite un número 0-99");
        int u = n % 10;
        int d = n / 10;

        switch (d) {
            case 1:
                switch (u) {
                    case 0:
                        System.out.println("diez");
                        break;
                    case 1:
                        System.out.println("once");
                        break;
                    case 2:
                        System.out.println("doce");
                        break;
                    case 3:
                        System.out.println("trece");
                        break;
                    case 4:
                        System.out.println("catorce");
                        break;
                    case 5:
                        System.out.println("quince");
                        break;
                    case 6:
                        System.out.println("dieciséis");
                        break;
                    case 7:
                        System.out.println("diecisiete");
                        break;
                    case 8:
                        System.out.println("dieciocho");
                        break;
                    case 9:
                        System.out.println("diecinueve");
                        break;
                }
                u = -1;
                break; //Reemplazar por un return, pero me saca del método
            case 2:
                System.out.print("Veinte");
                break;
            case 3:
                System.out.print("Treinta");
                break;
            case 4:
                System.out.print("Cuarenta");
                break;
            case 5:
                System.out.print("Cincuenta");
                break;
            case 6:
                System.out.print("Sesenta");
                break;
            case 7:
                System.out.print("Setenta");
                break;
            case 8:
                System.out.print("Ochenta");
                break;
            case 9:
                System.out.print("Noventa");
                break;
        }

        if (d > 0 && u != 0) {
            System.out.print(" y ");
        }

        switch (u) {
            case 0:
                if (d == 0) {
                    System.out.println("cero");
                }
                break;
            case 1:
                System.out.println("uno");
                break;
            case 2:
                System.out.println("dos");
                break;
            case 3:
                System.out.println("tres");
                break;
            case 4:
                System.out.println("cuatro");
                break;
            case 5:
                System.out.println("cinco");
                break;
            case 6:
                System.out.println("seis");
                break;
            case 7:
                System.out.println("siete");
                break;
            case 8:
                System.out.println("ocho");
                break;
            case 9:
                System.out.println("nueve");
                break;
        }
        System.out.println("\nGracias!!");
    }
}
