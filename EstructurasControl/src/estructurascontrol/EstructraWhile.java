/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructraWhile {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static void main(String[] args) {
        int variable = 1;
        System.out.println("1 ... 10\n");
        while (variable <= 10) {
            System.out.println(variable);
            variable++;
        }

        System.out.println("\n10 ... 1\n");
        variable = 10;
        while (variable >= 1) {
            System.out.println(variable);
            variable--;
        }

        System.out.println("\nTablas de Multiplicar\n");
        int tabla = leerInt("Digite la tabla de multiplicar");
        int cont = 1;
        while (cont <= 10) {
            int res = cont * tabla;
            System.out.println(tabla + "x" + cont + "=" + res);
            cont++;
        }

        System.out.println("\nSumatoria\n");
        cont = 1;
        int sum = 0;
        while (cont <= 10) {
            int n = leerInt(cont + ". Digite un número o -1");

            if (n == -1) {
                break;
            }

            sum += n;
            cont++;
        }
        System.out.println("\u2211 = " + sum);

        System.out.println("\nWhile Infinito\n");
        while (true) {
            int n = leerInt("Digite 0 para salir");
            if (n == 0) {
                break;
            }
            System.out.println("Hace algo!!");
        }

    }
}


//6+2+4+6+3=21
