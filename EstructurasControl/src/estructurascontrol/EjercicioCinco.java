/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioCinco {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Digite la cantidad de canciones: ");
        int canC = Integer.parseInt(sc.nextLine());
        System.out.print("Digite la cantidad de partituras: ");
        int canP = Integer.parseInt(sc.nextLine());

        if (canC >= 7 && canC <= 10) {
            if (canP == 0) {
                System.out.println("Músico naciente");
            } else if (canP >= 1 && canP <= 5) {
                System.out.println("Músico estelar");
            } else {
                System.out.println("Músico en formación");
            }
        } else if (canC > 10 && canP > 5) {
            System.out.println("Músico consagrado");
        } else {
            System.out.println("Músico en formación");
        }

    }
}
