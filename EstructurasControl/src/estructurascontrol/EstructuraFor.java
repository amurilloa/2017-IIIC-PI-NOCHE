/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import static estructurascontrol.EstructraWhile.leerInt;
import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraFor {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static void main(String[] args) {
        int tabla = leerInt("Digite la tabla");

        for (int i = 1; i <= 10; i++) {
            int res = tabla * i;
            System.out.println(tabla + "x" + i + "=" + res);
        }

        for (int i = 1; i <= 1000; i++) {
            if(i%7==0){
                System.out.println(i);
            }
        }
    }
}
