/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioSeis {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite el monto a retirar ");
        int monto = Integer.parseInt(sc.nextLine());

        if (monto >= 500) {
            int can = monto / 500;
            if (can == 1) {
                System.out.println(can + " billete de 500");
            } else {
                System.out.println(can + " billetes de 500");
            }
            monto %= 500;
        }
        if (monto >= 200) {
            System.out.println(monto / 200 + " billtes de 200");
            monto %= 200;
        }
        if (monto >= 100) {
            System.out.println(monto / 100 + " billtes de 100");
            monto %= 100;
        }
        if (monto >= 50) {
            System.out.println(monto / 50 + " billtes de 50");
            monto %= 50;
        }
        if (monto >= 20) {
            System.out.println(monto / 20 + " billtes de 20");
            monto %= 20;
        }
        int moneda = 10;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " billtes de " + moneda);
            monto %= moneda;
        }

        moneda = 5;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " billtes de " + moneda);
            monto %= moneda;
        }

        moneda = 2;
        if (monto >= moneda) {
            System.out.println(monto / moneda + " monedas de " + moneda);
            monto %= moneda;
        }

        moneda = 1;
        if (monto >= moneda) {
            System.out.println(monto + " moneda de " + moneda);
        }

    }

}
