/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class WhileCalc {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static char leerChar(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine().charAt(0);
    }

    public static void main(String[] args) {

        String menu = "\n\tMENU PRINCIPAL\n"
                + "+ Sumar\n"
                + "- Restar\n"
                + "* Multiplicar\n"
                + "/ Dividir\n"
                + "s Salir\n"
                + "Seleccione una opción";

        while (true) {
            char op = leerChar(menu);
            if (op == 's' || op == 'S') {
                break;
            }

            int n1 = leerInt("Digite un número");
            int n2 = leerInt("Digite otro número");

            switch (op) {
                case '+':
                    System.out.println(n1 + "+" + n2 + "=" + (n1 + n2));
                    break;
                case '-':
                    System.out.println(n1 + "-" + n2 + "=" + (n1 - n2));
                    break;
                case '*':
                    System.out.println(n1 + "x" + n2 + "=" + (n1 * n2));
                    break;
                case '/':
                    if (n2 != 0) {
                        double res = (double) n1 / n2;
                        res = Math.round(res * 100) / 100.0;
                        System.out.println(n1 + "/" + n2 + "=" + res);
                    } else {
                        System.out.println("Error, división por 0");
                    }
                    break;
            }
        }
    }
}
