/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EstructuraDoWhile {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static char leerChar(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine().charAt(0);
    }

    public static void main(String[] args) {
        int valor = 0;
        do {
            valor = leerInt("Digite un número 0 a 10");
        } while (valor < 0 || valor > 10);

        System.out.println(Math.pow(valor, 2));

    }
}
