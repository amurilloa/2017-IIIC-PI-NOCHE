/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioTres {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //Datos necesario para funcionar
        System.out.println("Digite la cantidad pasajeros");
        int canP = Integer.parseInt(sc.nextLine());
        System.out.println("Digite la cantidad ejes");
        int canE = Integer.parseInt(sc.nextLine());
        System.out.println("Digite el costo del vehículo");
        double costo = Double.parseDouble(sc.nextLine());

        double montoImp = costo * 0.01;
        double impE = 0;
        double impP = 0;

        if (canP < 20) {
            impP = montoImp * 0.01;
        } else if (canP >= 20 && canP <= 60) {
            impP = montoImp * 0.05;
        } else {
            impP = montoImp * 0.08;
        }

        if (canE == 2) {
            impE = montoImp * 0.05;
        } else if (canE == 3) {
            impE = montoImp * 0.10;
        } else if (canE > 3) {
            impE = montoImp * 0.15;
        }

        double costoTotal = costo + montoImp + impE + impP;
        System.out.println(costoTotal);
    }
}
