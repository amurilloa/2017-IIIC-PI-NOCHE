/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import java.util.Scanner;

/**
 *
 * @author ALLAN
 */
public class EjercicioSwitchCalc {

    public static int leerInt(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return Integer.parseInt(sc.nextLine());
    }

    public static char leerChar(String mensaje) {
        Scanner sc = new Scanner(System.in);
        System.out.print(mensaje + ": ");
        return sc.nextLine().charAt(0);
    }

    public static void main(String[] args) {
        int n1 = leerInt("Digite el n1");
        int n2 = leerInt("Digite el n2");
        char op = leerChar("Digite la operación (+,-,/,*)");

        switch (op) {
            case '+':
                System.out.println(n1 + "+" + n2 + "=" + (n1 + n2));
                break;
            case '-':
                System.out.println(n1 + "-" + n2 + "=" + (n1 - n2));
                break;
            case '*':
                System.out.println(n1 + "x" + n2 + "=" + (n1 * n2));
                break;
            case '/':
                if (n2 != 0) {
                    double res = (double) n1 / n2;
                    res = Math.round(res*100)/100.0;
                    System.out.println(n1 + "/" + n2 + "=" + res);
                } else {
                    System.out.println("Error, división por 0");
                }
                break;

        }

    }
}


//3.547*100= 354.7 = 355/100 = 3.55