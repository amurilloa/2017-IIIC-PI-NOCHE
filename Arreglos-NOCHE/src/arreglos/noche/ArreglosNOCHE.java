/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.noche;

import quiz.Persona;

/**
 *
 * @author ALLAN
 */
public class ArreglosNOCHE {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
//        int[] arreglo = new int[6];
//
//        int arreglo2[] = new int[6];
//
//        arreglo[0] = 19;
//        arreglo[1] = 1;
//        arreglo[2] = 39;
//        arreglo[3] = 5;
//        arreglo[4] = 53;
//        arreglo[5] = 15;
//        //arreglo[6] = 15; //ArrayIndexOutOfBoundsException: 6
//        for (int i = 0; i < arreglo.length; i++) {
//            System.out.println(arreglo[i]);
//        }
//
//        //Ultimo elemento del arreglo.
//        System.out.println(arreglo[arreglo.length - 1]);
//
//        //0, 1 ,2, 3 ,4, 5
//        int[] numeros = {1, 3, 4, 2, 4, 65};
//        for (int i = 0; i < numeros.length; i++) {
//            System.out.print(numeros[i] + " ");
//        }
//        String[] dias = {"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"};
//
//        System.out.println("");

//        Persona[] personas = new Persona[5];
//        personas[3] = new Persona("Allan", 29, 'H');
//
//        Persona p = new Persona("Juan", 14, 'H');
//        personas[1] = p;
//
//        for (int i = 0; i < personas.length; i++) {
//            if (personas[i] != null) {
//                System.out.println(personas[i].getNombre());
//            }
//        } 

        int x = Util.leerInt("Digite un número");

        Logica log = new Logica();
        int[] enteros = {2, 3, 4, 8, 1, 6};
        System.out.println("Promedio: " + log.promedio(enteros));
        System.out.println("Mayor: " + log.mayor(enteros));
        System.out.println("Suma Imp: " + log.sumaImp(enteros));
        System.out.println("Original: " + log.imprimir(enteros));
        int[] invertido = log.invertir(enteros);
        System.out.println("Invertido: " + log.imprimir(invertido));
        log.invertirDos(enteros);
        System.out.println("Invertido: " + log.imprimir(enteros));
        log.rotar(enteros);
        System.out.println("Rotar: " + log.imprimir(enteros));
        log.rotar(enteros);
        System.out.println("Rotar: " + log.imprimir(enteros));
        log.rotar(enteros);
        System.out.println("Rotar: " + log.imprimir(enteros));
        log.rotar(enteros);
        System.out.println("Rotar: " + log.imprimir(enteros));
        log.rotar(enteros);
        System.out.println("Rotar: " + log.imprimir(enteros));
        log.rotar(enteros);
        System.out.println("Rotar: " + log.imprimir(enteros));

    }

}
