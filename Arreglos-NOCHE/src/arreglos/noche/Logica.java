/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos.noche;

import quiz.Persona;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Persona[] personas;

    public Logica() {
        personas = new Persona[50];
    }

    public void registrar(Persona persona) {    
//        personas[x] = persona; ???
    }

    /**
     *
     * @param arreglo
     * @return
     */
    public double promedio(int[] arreglo) {
        double sumatoria = 0;
        for (int i = 0; i < arreglo.length; i++) {
            sumatoria += arreglo[i];
        }
        return sumatoria / arreglo.length;
    }

    /**
     * Obejetivo del método
     *
     * @param arreglo ¿Que es este parámetro?
     * @return ¿Que retorna?
     */
    public int mayor(int[] arreglo) {
        int mayor = arreglo[0];
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] > mayor) {
                mayor = arreglo[i];
            }
        }
        return mayor;
    }

    /**
     * Suma los elementos que son impares en el arreglo
     *
     * @param arreglo arreglo de enteros a evaluar
     * @return int con la suma de números impares del arreglo
     */
    public int sumaImp(int[] arreglo) {
        int sumatoria = 0;
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] % 2 != 0) {
                sumatoria += arreglo[i];
            }
        }
        return sumatoria;
    }

    /**
     *
     * @param elemento
     * @param arreglo
     * @return
     */
    public boolean buscar(int elemento, int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return true;
            }
        }
        return false;
    }

    /**
     * Busca la posición de un elemento en el arreglo
     *
     * @param elemento Elemento a buscar en el arreglo
     * @param arreglo Arreglo de elementos
     * @return int subindice del elemento o -1 si no lo encuentra
     */
    public int buscarPos(int elemento, int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return i;
            }
        }
        return -1;
    }
    //    {1,2,3,4,5,6} -> {6,5,4,3,2,1}

    //    {1,2,3,4,5,6}
    public int[] invertir(int[] arreglo) {
        int[] invertido = new int[arreglo.length];
        int k = arreglo.length - 1;
        for (int i = 0; i < arreglo.length; i++) {
            invertido[k--] = arreglo[i];
        }
        return invertido;
    }

    public void invertirDos(int[] arreglo) {
        int k = arreglo.length - 1;
        for (int i = 0; i < arreglo.length; i++) {
            if (k <= i) {
                break;
            }
            int c = arreglo[i];
            arreglo[i] = arreglo[k];
            arreglo[k] = c;
            k--;
        }
    }

//    {6,1,2,3,4,5}
    public void rotar(int[] arreglo) {
        int ult = arreglo[arreglo.length - 1];
        for (int i = arreglo.length - 2; i >= 0; i--) {
            arreglo[i + 1] = arreglo[i];
        }
        arreglo[0] = ult;
    }

    public String imprimir(int[] arreglo) {
        String mensaje = "";
        for (int i = 0; i < arreglo.length; i++) {
            mensaje += arreglo[i] + " ";
        }
        return mensaje;
    }
}
