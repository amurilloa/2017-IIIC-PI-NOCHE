/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

/**
 *
 * @author ALLAN
 */
public class Persona {

    private String nombre;
    private int edad;
    private String cedula;
    private char sexo;

    private final char HOM = 'H';
    private final char MUJ = 'M';

    public Persona() {
        cedula = generarCedula();
        nombre = "";
        edad = 0;
        sexo = HOM;
    }

    public Persona(String nombre, int edad, char sexo) {
        cedula = generarCedula();
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = comprobarSexo(sexo);
    }

    public Persona(String nombre, int edad, String cedula, char sexo) {
        this.nombre = nombre;
        this.edad = edad;
        this.cedula = cedula;
        this.sexo = comprobarSexo(sexo);
    }

    /**
     * Genera un String con 9 numeros aleatorios
     *
     * @return String con número de cédula
     */
    public String generarCedula() {
        String cedula = "";
        for (int i = 0; i < 10; i++) {
            int n = (int) (Math.random() * 9) + 1;
            cedula += n;
        }
        return cedula;
    }

    /**
     * Determina si el sexo asignado es correcto
     *
     * @param sexo Sexo a evaluar
     * @return Sexo correcto
     */
    private char comprobarSexo(char sexo) {
        if (sexo == HOM || sexo == MUJ) {
            return sexo;
        }
        return HOM;
    }

    /**
     * Determina si una persona es mayor de edad Mayor Edad >= 18
     *
     * @return True en caso de ser mayor de edad
     */
    public boolean esMayorEdad() {
        return edad >= 18;
    }

    /**
     *
     * @param peso
     * @param altura
     * @return
     */
    public int calcularIMC(double peso, double altura) {
        //peso en kg/(altura^2  en m)        
        double imc = peso / (Math.pow(altura, 2));
        if (imc > 25) {
            return 1;
        } else if (imc >= 20) {
            return 0;
        } else {
            return -1;
        }
    }

    public String analisisIMC(int imc) {
        String msj = "";
        if (imc == 0) {
            msj = "Peso ideal";
        } else if (imc == 1) {
            msj = "Sobrepeso";
        } else {
            msj = "Bajo peso";
        }
        return msj;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCedula() {
        return cedula;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = comprobarSexo(sexo);
    }

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", edad=" + edad + ", cedula=" + cedula + ", sexo=" + sexo + '}';
    }

   
}
