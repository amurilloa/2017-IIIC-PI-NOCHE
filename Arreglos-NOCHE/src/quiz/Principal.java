/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz;

import arreglos.noche.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        String n = Util.leerString("Nombre");
        int e = Util.leerInt("Edad");
        char s = Util.leerString("Sexo").charAt(0);
        double p = Util.leerDouble("Peso(kg)");
        double a = Util.leerDouble("Estatura(m)");

        Persona p1 = new Persona(n, e, "1231232", s);

        Persona p2 = new Persona(n, e, s);

        Persona p3 = new Persona();

        p3.setNombre(n);
        p3.setEdad(e);

        int imc = p1.calcularIMC(p, a);
        System.out.println(p1.analisisIMC(imc));
        imc = p2.calcularIMC(p, a);
        System.out.println(p2.analisisIMC(imc));
        imc = p3.calcularIMC(p, a);
        System.out.println(p3.analisisIMC(imc));
        System.out.println(p1.esMayorEdad() ? "Es mayor" : "Es menor");
        System.out.println(p2.esMayorEdad() ? "Es mayor" : "Es menor");
        System.out.println(p3.esMayorEdad() ? "Es mayor" : "Es menor");

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
    }
}
