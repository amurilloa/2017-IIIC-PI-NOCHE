/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tutoria;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Perro[] perrera;

    public Logica() {
        perrera = new Perro[5];
    }

    public void registrarPerro(Perro p) {
        for (int i = 0; i < perrera.length; i++) {
            if (perrera[i] == null) {
                perrera[i] = p;
                break;
            }
        }
    }

    public String imprimirPerrera() {
        String mensaje = "";
        for (int i = 0; i < perrera.length; i++) {
            if (perrera[i] != null) {
                mensaje += perrera[i].getNombre() + "\n";
            } else {
                mensaje += "Disponible\n";
            }
        }
        return mensaje;
    }

    public int suma(int[] nombre) {
        int suma = 0;
        for (int i = 0; i < nombre.length; i++) {
            suma = suma + nombre[i];
        }
        return suma;
    }
}
