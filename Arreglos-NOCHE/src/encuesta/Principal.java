/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

import arreglos.noche.Util;

/**
 *
 * @author ALLAN
 */
public class Principal {

    public static void main(String[] args) {
        Encuesta e = new Encuesta();
        //Datos de prueba
        e.registrar(new Estudiante(1, 1, 1, 100000));
        e.registrar(new Estudiante(2, 2, 1, 100000));
        e.registrar(new Estudiante(3, 2, 0, 0));
        e.registrar(new Estudiante(4, 1, 1, 130000));
        e.registrar(new Estudiante(5, 2, 1, 150000));

        String menu = "\n   Menu Principal\n"
                + "1. Registrar Estudiante\n"
                + "2. Finalizar Encuesta\n"
                + "3. Salir\n"
                + "Seleccione una opción";

        String subMenu = "\n   Analisis de datos\n"
                + "1. Porcentaje de Hombres\n"
                + "2. Porcentaje de Mujeres\n"
                + "5. Volver\n"
                + "Seleccione una opción";

        while (true) {
            int op = Util.leerInt(menu);
            if (op == 3) {
                break;
            }
            if (op == 1) {
                Estudiante est = new Estudiante();
                est.setCedula(Util.leerInt("Cédula"));
                est.setSexo(Util.leerInt("Sexo: \n1. Hombre\n2. Mujer"));
                est.setTrabaja(Util.leerInt("Trabaja: \n1. SI\n2. No"));
                if (est.getTrabaja() == 1) {
                    est.setSueldo(Util.leerInt("Sueldo"));
                }
                e.registrar(est);
            } else if (op == 12) {
                System.out.println(e.imprimir());
            } else if (op == 2) {
                while (true) {
                    op = Util.leerInt(subMenu);
                    if (op == 5) {
                        break;
                    } else if (op == 1) {
                        System.out.println(e.porcentaje(1));
                    } else if (op == 2) {
                        System.out.println(e.porcentaje(2));
                    }
                }
            }
        }

    }
}
