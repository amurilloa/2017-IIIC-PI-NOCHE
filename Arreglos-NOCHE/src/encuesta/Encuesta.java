/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuesta;

/**
 *
 * @author ALLAN
 */
public class Encuesta {

    private Estudiante[] estudiantes;

    public Encuesta() {
        estudiantes = new Estudiante[50];
        estudiantes[0] = new Estudiante(123, 2, 1, 12312);
        estudiantes[1] = new Estudiante(123, 2, 1, 12312);
    }

    public void registrar(Estudiante est) {
        for (int i = 0; i < estudiantes.length; i++) {
            if (estudiantes[i] == null) {
                estudiantes[i] = est;
                break;
            }
        }
    }

    /**
     * Calcula el porcentaje de hombres o mujeres registrados en la encuesta
     *
     * @param sexo Sexo que se requiere
     * @return
     */
    public double porcentaje(int sexo) {
        int total = 0;
        int cantS = 0;
//        {asd, asd, asd, null, null, null}
        for (int i = 0; i < estudiantes.length; i++) {
            if (estudiantes[i] != null) {
                total++;
                if (estudiantes[i].getSexo() == sexo) {
                    cantS++;
                }
            }
        }
        return cantS * 100.0 / total;
    }

    public String imprimir() {
        String mensaje = "";
        for (int i = 0; i < estudiantes.length; i++) {
            mensaje += estudiantes[i] + "\n";
        }
        return mensaje;
    }

}
