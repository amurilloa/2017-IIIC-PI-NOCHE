/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author ALLAN
 */
public class PanelJuego extends JPanel implements KeyListener {

    private Logica log;

    public PanelJuego() {
        setPreferredSize(new Dimension(900, 900));
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 6));
        addKeyListener(this);
        setFocusable(true);
        log = new Logica();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        //Fondo
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 900, 900);
        log.paint(g);
        log.mover();
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            log.setDireccion(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            log.setDireccion(2);
        } else if (ke.getKeyCode() == KeyEvent.VK_UP) {
            log.setDireccion(3);
        } else if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            log.setDireccion(4);
        } else if (ke.getKeyCode() == KeyEvent.VK_P) {
            log.setDireccion(0);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
