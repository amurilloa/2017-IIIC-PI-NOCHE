/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author ALLAN
 */
public class Logica {

    private Circulo manzana;
    private LinkedList<Circulo> serpiente;
    private int direccion;//1 der, 2 izq, 3 arriba, 4 abajo

    public Logica() {
        serpiente = new LinkedList<>();
        config();
    }

    private void config() {
        manzana = new Circulo(450, 450, Color.GREEN, 15);
        serpiente.add(new Circulo(450, 480, Color.BLUE, 15));
        serpiente.add(new Circulo(450, 510, Color.GRAY, 15));
        serpiente.add(new Circulo(450, 540, Color.GRAY, 15));
    }

    public Circulo getManzana() {
        return manzana;
    }

    public void paint(Graphics g) {
        manzana.paint(g);
//        for (Circulo x : serpiente) { //for  x in [q,s,d,e]:
//            x.paint(g);
//        }
        for (int i = 0; i < serpiente.size(); i++) {
            serpiente.get(i).paint(g);
        }
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public void mover() {

        Circulo cabeza = serpiente.getFirst();
//        if (cabeza.getX() == manzana.getX() && cabeza.getY() == manzana.getY()) {
        if (cabeza.getBounds().intersects(manzana.getBounds())) {
            System.out.println("Pasó por la manzana");
            System.out.println("no quite la cola");
//            manzana.setX((int) (Math.random() * 870));
//            manzana.setY((int) (Math.random() * 870));

            manzana.setX(((int) (Math.random() * 29)) * 30);
            manzana.setY(((int) (Math.random() * 29)) * 30);
            //Rifar la manzana
        } else if (direccion > 0) {
            serpiente.removeLast();
        }

        if (direccion == 1) {
            serpiente.getFirst().setColor(Color.GRAY);
            Circulo nuevo = new Circulo(serpiente.getFirst().getX() + 30,
                    serpiente.getFirst().getY(),
                    Color.BLUE, 15);
            serpiente.addFirst(nuevo);
        } else if (direccion == 2) {
            serpiente.getFirst().setColor(Color.GRAY);
            Circulo nuevo = new Circulo(serpiente.getFirst().getX() - 30,
                    serpiente.getFirst().getY(),
                    Color.BLUE, 15);
            serpiente.addFirst(nuevo);
        } else if (direccion == 3) {
            serpiente.getFirst().setColor(Color.GRAY);
            Circulo nuevo = new Circulo(serpiente.getFirst().getX(),
                    serpiente.getFirst().getY() - 30,
                    Color.BLUE, 15);
            serpiente.addFirst(nuevo);
        } else if (direccion == 4) {
            serpiente.getFirst().setColor(Color.GRAY);
            Circulo nuevo = new Circulo(serpiente.getFirst().getX(),
                    serpiente.getFirst().getY() + 30,
                    Color.BLUE, 15);
            serpiente.addFirst(nuevo);
        }

    }

}
