/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author ALLAN
 */
public class Circulo {

    private int x;
    private int y;
    private Color color;
    private int radio;

    public Circulo() {
    }

    public Circulo(int x, int y, Color color, int radio) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.radio = radio;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, radio * 2, radio * 2);
    }

    public Rectangle getBounds() {
        Rectangle temp = new Rectangle(x, y, radio * 2, radio * 2);
        return temp;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    @Override
    public String toString() {
        return "Circulo{" + "x=" + x + ", y=" + y + ", color=" + color + ", radio=" + radio + '}';
    }

}
