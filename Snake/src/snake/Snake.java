/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package snake;

import javax.swing.JFrame;

/**
 *
 * @author ALLAN
 */
public class Snake {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame("Snake v1.0");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Crear panel y agregarlo al formulario
        frm.add(new PanelJuego());
        frm.pack();
        frm.setLocationRelativeTo(null);
        frm.setVisible(true);
        while (true) {
            frm.repaint();
            Thread.sleep(200);
        }

    }

}
