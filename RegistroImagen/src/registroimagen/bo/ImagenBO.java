/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registroimagen.bo;

import java.util.LinkedList;
import registroimagen.dao.ImagenDAO;
import registroimagen.entities.Imagen;
import registroimagen.entities.MiError;

/**
 *
 * @author ALLAN
 */
public class ImagenBO {

    public void registrar(Imagen img) {
        if (img.getTitulo().isEmpty()) {
            throw new MiError("Favor especificar el título");
        }
        if (img.getTiempo() <= 0 && img.getTiempo() > 100) {
            throw new MiError("Favor especificar un tiempo entre 1-100");
        }
        if (img.getImagen() == null) {
            throw new MiError("Favor seleccionar una imagen");
        }
        ImagenDAO dao = new ImagenDAO();
        dao.insertar(img);
    }

    public LinkedList<Imagen> cargarImagenes() {
        ImagenDAO dao = new ImagenDAO();
        return dao.cargarTodo();

    }

    public void eliminar(int id) {
        if (id <= 0) {
            throw new MiError("Favor seleccionar una imagen");
        }
        ImagenDAO dao = new ImagenDAO();
        dao.eliminar(id);
    }

}
