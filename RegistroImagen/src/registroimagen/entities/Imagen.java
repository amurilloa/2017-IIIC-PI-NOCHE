/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registroimagen.entities;

import java.awt.Image;

/**
 *
 * @author ALLAN
 */
public class Imagen {

    private int id;
    private String titulo;
    private int tiempo;
    private Image imagen;

    public Imagen() {
    }

    public Imagen(int id, String titulo, int tiempo, Image imagen) {
        this.id = id;
        this.titulo = titulo;
        this.tiempo = tiempo;
        this.imagen = imagen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public Image getImagen() {
        return imagen;
    }

    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return id + ". " + titulo;
    }

}
