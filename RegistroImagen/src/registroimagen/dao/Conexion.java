/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registroimagen.dao;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import registroimagen.entities.MiError;

/**
 *
 * @author ALLAN
 */
public class Conexion {

    public static final String DRIVER = "jdbc:postgresql://";
    public static final String SERVER = "localhost:5433/";
    public static final String DB = "visordb";
    public static final String USER = "postgres";
    public static final String PASS = "postgres";

    public static Connection getConexion() {
        Connection conn = null;
        String url = DRIVER + SERVER + DB;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, USER, PASS);
            System.out.println("Conexión  con éxito!!"); //TODO: Eliminar linea
        } catch (ClassNotFoundException ex) {
            throw new MiError("Falta el driver de base de datos");
        } catch (SQLException ex) {
            throw new MiError("Problemas al realizar la conexión\n" + ex.getMessage());
        }
        return conn;
    }

    public static void main(String[] args) throws IOException, SQLException {
        File pathToFile = new File("Lucy.jpg");
        Image image = ImageIO.read(pathToFile);

        Connection con = getConexion();
//        String sql = "insert into imagen(imagen, tiempo, titulo) values(?,?,?)";
//        PreparedStatement stmt = con.prepareStatement(sql);
//        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        ImageIO.write((RenderedImage) image, "jpg", os);
//        InputStream fis = new ByteArrayInputStream(os.toByteArray());
//        stmt.setBinaryStream(1, fis);
//        stmt.setInt(2, 8);
//        stmt.setString(3, "Imagen de Prueba");
//        System.out.println(stmt.executeUpdate());
        JFrame frm = new JFrame();
        JLabel lbl = new JLabel();
        String sql = "select * from imagen where id = 1";
        PreparedStatement stmt = con.prepareStatement(sql);
        ResultSet rs = stmt.executeQuery();
        Image imgdb = null;
        if (rs.next()) {
            InputStream fis = rs.getBinaryStream("imagen");
            imgdb = ImageIO.read(fis);
        }
        lbl.setIcon(new ImageIcon(imgdb));
        frm.add(lbl);
        frm.pack();
        frm.setVisible(true);

    }
}
