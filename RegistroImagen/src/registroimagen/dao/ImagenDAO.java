/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registroimagen.dao;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import registroimagen.entities.Imagen;
import registroimagen.entities.MiError;

/**
 *
 * @author ALLAN
 */
public class ImagenDAO {

    public void insertar(Imagen img) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "insert into imagen(titulo, tiempo, imagen) values (?,?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, img.getTitulo());
            stmt.setInt(2, img.getTiempo());
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write((RenderedImage) img.getImagen(), "jpg", os);
            InputStream fis = new ByteArrayInputStream(os.toByteArray());
            stmt.setBinaryStream(3, fis);
            stmt.executeUpdate();
        } catch (Exception ex) {
            throw new MiError("Problemas con la base de datos.");
        }
    }

    public LinkedList<Imagen> cargarTodo() {
        LinkedList<Imagen> imagenes = new LinkedList<>();
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from imagen";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                imagenes.add(cargarImagen(rs));
            }
            return imagenes;
        } catch (Exception ex) {
            throw new MiError("Problemas con la base de datos.");
        }
    }

    private Imagen cargarImagen(ResultSet rs) throws SQLException, IOException {
        Imagen img = new Imagen();
        img.setId(rs.getInt("id"));
        img.setTitulo(rs.getString("titulo"));
        img.setTiempo(rs.getInt("tiempo"));
        Image imgdb = null;
        InputStream fis = rs.getBinaryStream("imagen");
        imgdb = ImageIO.read(fis);
        img.setImagen(imgdb);
        return img;
    }

    public void eliminar(int id) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "delete from imagen where id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (Exception ex) {
            throw new MiError("Problemas con la base de datos.");
        }
    }

}
